﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController2D : MonoBehaviour
{
    //public static CameraController Instance;

    [SerializeField] GameObject Target = null;
    [SerializeField] int Smoothvalue =2;
    [SerializeField] float posY = 1;
    [SerializeField] float yMinHeight;
    [SerializeField] float yMaxHeight;
    float targPosY;

    // Update is called once per frame
    void Update()
    {
        targPosY = Target.transform.position.y;
        
        if(targPosY < yMinHeight)
            targPosY = 0;
        if(targPosY > yMaxHeight)
            targPosY = transform.position.y;

        Vector3 Targetpos = new Vector3(Target.transform.position.x, targPosY + posY, -10);
        transform.position = Vector3.Lerp(transform.position, Targetpos, Time.deltaTime * Smoothvalue);
    }
}
