﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUI : MonoBehaviour
{
    [SerializeField] PlayerHealth playerHealth;
    [SerializeField] float minScale = 0.8f;
    float currScale;
    bool maximize, cofeeIncrase;
    private int currHealth;
    GameObject heart1, heart2, heart3;
    float speed;
    [SerializeField] float coffeeSpeed, normSpeed;

    // Start is called before the first frame update
    void Start()
    {
        maximize = false;

        //TODO: Cambiar esta línea
        cofeeIncrase = false;

        // Get the hearts gameobjects
        heart1 = transform.Find("H1").gameObject;
        heart2 = transform.Find("H2").gameObject;
        heart3 = transform.Find("H3").gameObject;

        currScale = heart1.transform.localScale.x;
        StartCoroutine(Scale());
    }

    private void Update()
    {
        cofeeIncrase = !playerHealth.IsNormalIncrease();

        if (currHealth != playerHealth.GetHealth())
        {   
            currHealth = playerHealth.GetHealth();     

            if (currHealth >= 3)
                heart3.SetActive(true);
            else
                heart3.SetActive(false);

            if (currHealth >= 2)
                heart2.SetActive(true);
            else
                heart2.SetActive(false);

            if (currHealth >= 1)
                heart1.SetActive(true);
            else
                heart1.SetActive(false);
        }

        currHealth = playerHealth.GetHealth();
    }

    IEnumerator Scale()
    {
        while (true)
        {
            // Set the speed of the increase
            speed = cofeeIncrase ? coffeeSpeed : normSpeed;

            if (maximize)
            {
                currScale += 0.01f * speed * Time.deltaTime;
                if (heart1.transform.localScale.x > 1.0f) maximize = false;
            }
            else
            {
                currScale -= 0.01f * speed * Time.deltaTime;
                if (heart1.transform.localScale.x < minScale) maximize = true;
            }

            heart1.transform.localScale = Vector2.one * currScale;
            heart2.transform.localScale = Vector2.one * currScale;
            heart3.transform.localScale = Vector2.one * currScale;

            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
}
