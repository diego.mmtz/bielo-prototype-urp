﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    enum State { Patrol, Chasing, Attack }

    [Header("Patrol movement")]
    [SerializeField] Transform[] moveSpots = null;
    [SerializeField] Transform groundDetection = null;
    [SerializeField] float targetRange = 3f;
    [SerializeField] float speed = 2.5f;
    [SerializeField] float startWaitTime = 1f;
    [SerializeField] float groundDistance = 1f;
    [SerializeField] float stopChaseDistance = 8f;

    Animator anim = null;
    int currentSpot;
    float waitTime;
    State state;
    bool canMove;
    bool facingRight = true;
    Vector3 desiredPos;

    void Start()
    {
        anim = GetComponent<Animator>();
        state = State.Patrol;
        waitTime = startWaitTime;
    }

    void Update()
    {
        CheckMove();

        switch (state)
        {
            default:
            case State.Patrol:

                desiredPos = new Vector3(moveSpots[currentSpot].transform.position.x, transform.position.y, 0);
                // Check if the player has to look to the other side
                if (desiredPos.x < transform.position.x && facingRight)
                    Flip();
                else if (desiredPos.x > transform.position.x && !facingRight)
                    Flip();

                if (!canMove)
                {
                    currentSpot = (++currentSpot) % 2;
                    return;
                }

                transform.position = Vector2.MoveTowards(transform.position, desiredPos, speed * Time.deltaTime);

                if (Vector2.Distance(transform.position, desiredPos) < 0.2f)
                {
                    if (waitTime <= 0)
                    {
                        currentSpot = (++currentSpot) % 2;
                        waitTime = startWaitTime;
                    }
                    else
                    {
                        waitTime -= Time.deltaTime;
                        anim.SetBool("Run", false);
                    }
                }
                else
                {
                    anim.SetBool("Run", true);
                }
                FindTarget();                

                break;
            case State.Chasing:
                float attackRange = 1f;
                float playerDist = Vector2.Distance(transform.position, PlayerController2D.Instance.GetPosition());

                // Check if player gets out of "visible range"
                if (playerDist > stopChaseDistance){
                    state = State.Patrol;
                    return;
                }

                if (playerDist < attackRange)
                {
                    // Target inside attack range
                    anim.SetBool("Run", false);
                    
                    if(!anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                        anim.SetTrigger("Attack");
                }
                else
                {                
                    //Follow player as long as he can
                    if (!canMove)
                    {
                        anim.SetBool("Run", false);
                    }
                    else
                    {
                        anim.SetBool("Run", true);
                        desiredPos = new Vector3(PlayerController2D.Instance.GetPosition().x, transform.position.y, 0);
                        transform.position = Vector2.MoveTowards(transform.position, desiredPos, speed * Time.deltaTime);
                    }
                }

                if (PlayerController2D.Instance.GetPosition().x < transform.position.x && facingRight)
                    Flip();
                else if (PlayerController2D.Instance.GetPosition().x > transform.position.x && !facingRight)
                    Flip();
                break;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void CheckMove()
    {
        //Raycast to the ground to check if the player has ground to move
        RaycastHit2D raycast = Physics2D.Raycast(groundDetection.transform.position, Vector2.down, groundDistance);
        canMove = raycast.collider != null ? true : false;
    }


    void FindTarget()
    {
        if (Vector2.Distance(transform.position, PlayerController2D.Instance.GetPosition()) < targetRange)
        {
            // Player inside range
            state = State.Chasing;
        }
    }
}
