﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    [SerializeField] protected int currentHealth = 1;     // Current/initial health
    [SerializeField] protected int maxHealth;             // Max health

    // make current health percentage visible
    public float HealthPercentage => (float)currentHealth / maxHealth;

    // on death unity event
    [SerializeField] UnityEvent OnDeath;

    public void Damage(int amount)
    {
        // deal daamge
        currentHealth -= amount;

        if(currentHealth <= 0) Death();
    }

    void Death()
    {
        OnDeath?.Invoke();

        Destroy(gameObject);
    }
}
