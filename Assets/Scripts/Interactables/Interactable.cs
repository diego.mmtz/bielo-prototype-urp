﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    bool inRange;
    public UnityEvent startEndAction, entersRange, exitsRange, nextLine;

    // Update is called once per frame
    void Update()
    {
        if(inRange && Input.GetButtonDown("Interact"))
            startEndAction.Invoke();

        if(inRange && Input.GetKeyDown(KeyCode.Space))
            nextLine.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.CompareTag("Player"))
        {
            inRange = true;
            entersRange.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.CompareTag("Player"))
            inRange = false;
            exitsRange.Invoke();
    }
}
