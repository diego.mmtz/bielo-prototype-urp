﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatInteraction : MonoBehaviour
{
    [SerializeField] GameObject interactionMark =  null;
    [SerializeField] DialogueManager dialogueManager = null;
    bool isDialogueOpen = false;

    public Dialogue dialogue;

    public void ToogleDialogue()
    {
        if(isDialogueOpen)
            dialogueManager.EndDialogue();
        else
            dialogueManager.StartDialogue(dialogue);

        isDialogueOpen = !isDialogueOpen;
    }

    public void NextLine()
    {
        if(!isDialogueOpen) return;

        isDialogueOpen = dialogueManager.DisplayNextSentence();
    }

    public void ShowHideMark(bool show)
    {
        interactionMark.SetActive(show);
    }
}
