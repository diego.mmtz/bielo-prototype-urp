﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float runSpeed = 40f;

    PlayerController2D playerController;
    Animator anim;
    float horizontalMove = 0;
    bool jump;

    void Awake()
    {
        anim = GetComponent<Animator>();
        playerController = GetComponent<PlayerController2D>();
    }

    void Update()
    {
        ReadInput();
    }

    void ReadInput() {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        anim.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if(Input.GetButtonDown("Jump")){
            jump = true;
            anim.SetBool("isJumping", true);
        }

        if(Input.GetButtonDown("Attack") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Attack") && playerController.isGrounded){
            anim.SetTrigger("Attack");
        }
    }

    public void OnLanding() {
        anim.SetBool("isJumping", false);
    }

    private void FixedUpdate() {
        // Move the player
        playerController.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }
}
