﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController2D : MonoBehaviour
{
    public static PlayerController2D Instance { get; private set; }

    [SerializeField] float jumpForce = 400;
    [Range(0, .3f)] [SerializeField] private float movementSmoothing = .05f;	// How much to smooth out the movement
    [SerializeField] bool airControl = false;                                   // Mover el jugador en el aire
    public LayerMask groundLayer;   // Layer del suelo
    [SerializeField] Transform feetTransform = null;                            // Posición de los pies
    Animator anim;

    const float groundedDistance = .5f;     // Distancia para checar si se toca el suelo
    public bool isGrounded;                 // Checar si el personaje está tocando el suelo
    private Rigidbody2D rb;
    bool facingRight = true;                // Lado al que el personaje ve
    Vector3 velocity = Vector3.zero;

    [Header("Eventos")]
    [Space]
    [SerializeField] UnityEvent OnLandEvent;

    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }

    public BoolEvent OnCrouchEvent;
    //bool wasCrouching = false;

    public Vector3 GetPosition()
    {        
        return transform.position;
    }

    private void Awake()
    {
        Instance = this;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        if (OnLandEvent == null) OnLandEvent = new UnityEvent();
        if (OnCrouchEvent == null) OnCrouchEvent = new BoolEvent();
    }

    private void FixedUpdate()
    {
        bool wasGrounded = isGrounded;
        isGrounded = false;

        // Check for collision with ground (radius on feet)
        foreach (Collider2D col in Physics2D.OverlapCircleAll(feetTransform.position, groundedDistance, groundLayer))
        {
            // Check collision is not with myself
            if (col.gameObject != gameObject)
            {
                isGrounded = true;
                if (!wasGrounded)
                    OnLandEvent.Invoke();
            }
        }
    }

    public void Move(float move, bool crouch, bool jump)
    {
        // Move if I'm at the ground or I have airControl
        if (isGrounded || airControl)
        {
            // Move the character
            Vector3 targetVelocity = new Vector2(move * 10f, rb.velocity.y);
            // Smooth and apply
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, movementSmoothing);

            // Check which side is the player looking at
            if (move > 0 && !facingRight)
                Flip();
            else if (move < 0 && facingRight)
                Flip();
        }

        // Check for jump
        if (isGrounded && jump && !anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            isGrounded = false;
            rb.AddForce(new Vector2(0, jumpForce));
        }
    }

    private void Flip()
    {
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
