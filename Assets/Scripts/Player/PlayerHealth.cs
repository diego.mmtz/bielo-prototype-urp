﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : Health
{
    [SerializeField] float healNormalIncrease;
    [SerializeField] float healCofeeIncrease;
    
    float healIncrease;
    float healAmount;
    bool isHealing;

    private void Start() {
        isHealing = false;
        healIncrease = healNormalIncrease;
        healAmount = 0;
    }

    private void Update() {        
        // Check if healing is on
        if(currentHealth == maxHealth)
        {
            healAmount = 0;
            isHealing = false;
        }else{
            isHealing = true;
            healAmount += healIncrease * Time.deltaTime;
            //Debug.Log(healAmount);
        }

        // Increase health if 100%
        if(healAmount >= 100)
        {
            healAmount = 0;
            currentHealth++;
        }
    }

    public bool IsNormalIncrease() {
        return (healIncrease == healNormalIncrease);
    }

    public int GetHealth() {
        return currentHealth;
    }

    void Death()
    {
        Debug.Log("morí");
    }
}
